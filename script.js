const e = require("express");
const express = require("express");
const app = express();
const port = 3002;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const mongoose = require("mongoose");

mongoose.connect(
  "mongodb+srv://admin:admin@zuittbatch243.m7n9nfd.mongodb.net/?retryWrites=true&w=majority",

  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`We're connected to the cloud database`));

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username }, (err, result) => {
    if (result !== null && result.username === req.body.name) {
      return res.send("Username already exist!");
    } else {
      let newUser = new User({
        username: req.body.username,
        password: req.body.password,
      });

      newUser.save((saveErr, savedUser) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New user created");
        }
      });
    }
  });
});

// TO SEE ALL USERS
app.get("/users", (req, res) => {
  User.find({}, (err, result) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});

app.listen(port, () => console.log(`Server is running at localhost:${port}`));
